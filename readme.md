# Results Generation Test

This project is Adam Kelso's answer to the programming challenge set forth by Results Generation.

### The Following Assumptions Where Made:

- Data persistence was not necessary. As adding database access would require both migrations and a seeder and in almost all cases an abstracted layer. Instead, a "seeder" class has been provided in the tests folder that is called from the app whenever needed.
- Because a database abstraction layer has not been provided, the class located at **src\Entities\Sensors** is intended to be a fake of a ORM model. For example, if the app were built with Laravel, the Sensors would be an **Eloquent** model. In this case, the implementation simply loads an array of records from the seeder.
- The connection of services to sensors is beyond the scope of the test. Obviously, the internal workings of a given set of sensors was not explained in the challenge instructions, but three different service types were included just to show how multiple protocol types could be dynamically chosen based on the sensor.
- Likewise, the routing of this app is out of scope, so a routing package is included via composer.

### The Following Conventions Were Used:

- The **src** directory follows the same basic conventions of a framework, using PSR-4 Autoloading.
- Within the **src**, a basic MVC structure is used. However, because models can be rather misleading, the business logic of the app is split between multiple directories: **Services**, **Sensors**, and **Entities**. Each sub-directory is a closer approximation of the contained class' intent.
- **PHPUnit** is used as the testing framework, loaded via composer.
- The **Smarty** templating engine was used for views
- All views were simply built using **Bootstrap CSS**.

### Setup
Clone all code from the repository into a local directory and point a web server to the **public** directory. Depending on your local environment, you may need to modify the **.htaccess** file in the **public** directory.

After cloning, load all dependencies via composer:

```composer install```

Upon completion, the app and unit tests should run without issue.


