<?php

use App\Enums\ThresholdTypes;
use App\Sensors\{
    Temperature, Door, GlassBreak, SmokeDetector
};
use App\Services\{
    RESTful, FTP, Soap
};

$kitchen = new \stdClass();
$kitchen->id = 1;
$kitchen->name = 'Kitchen Temperature';
$kitchen->type = Temperature::class;
$kitchen->thresholds = [
    ThresholdTypes::Maximum => '130'
];
$kitchen->connection_type = RESTful::class;
$kitchen->url = 'http://localIP/kitchen';
$kitchen->credentials = [];
$kitchen->value = 70.6;

$basement = new \stdClass();
$basement->id = 2;
$basement->name = 'Basement Temperature';
$basement->type = Temperature::class;
$basement->thresholds = [
    ThresholdTypes::Minimum => '33'
];
$basement->connection_type = RESTful::class;
$basement->url = 'http://localIP/basement';
$basement->credentials = [];
$basement->value = 88.2;

$front = new \stdClass();
$front->id = 3;
$front->name = 'Front Door';
$front->type = Door::class;
$front->thresholds = [
    ThresholdTypes::Boolean => false
];
$front->connection_type = FTP::class;
$front->url = 'ftp://localIP/front';
$front->credentials = [];
$front->value = true;

$back = new \stdClass();
$back->id = 4;
$back->name = 'Back Door';
$back->type = Door::class;
$back->thresholds = [
    ThresholdTypes::Boolean => false
];
$back->connection_type = FTP::class;
$back->url = 'ftp://localIP/back';
$back->credentials = [];
$back->value = false;

$basement_glass = new \stdClass();
$basement_glass->id = 5;
$basement_glass->name = 'Basement';
$basement_glass->type = GlassBreak::class;
$basement_glass->thresholds = [
    ThresholdTypes::Maximum => 300
];
$basement_glass->connection_type = Soap::class;
$basement_glass->url = 'http://localIP/basement_glass';
$basement_glass->credentials = [];
$basement_glass->value = 225.84;

$main = new \stdClass();
$main->id = 6;
$main->name = 'Main Floor';
$main->type = GlassBreak::class;
$main->thresholds = [
    ThresholdTypes::Maximum => 300
];
$main->connection_type = Soap::class;
$main->url = 'http://localIP/main';
$main->credentials = [];
$main->value = 955.677;

$upstairs = new \stdClass();
$upstairs->id = 7;
$upstairs->name = 'Upstairs';
$upstairs->type = GlassBreak::class;
$upstairs->thresholds = [
    ThresholdTypes::Maximum => 300
];
$upstairs->connection_type = Soap::class;
$upstairs->url = 'http://localIP/upstairs';
$upstairs->credentials = [];
$upstairs->value = 166.592;

$bed1 = new \stdClass();
$bed1->id = 8;
$bed1->name = 'Bedroom 1';
$bed1->type = SmokeDetector::class;
$bed1->thresholds = [
    ThresholdTypes::Minimum => 70
];
$bed1->connection_type = RESTful::class;
$bed1->url = 'http://localIP/bed1';
$bed1->credentials = [];
$bed1->value = 7.4;

$bed2 = new \stdClass();
$bed2->id = 9;
$bed2->name = 'Bedroom 2';
$bed2->type = SmokeDetector::class;
$bed2->thresholds = [
    ThresholdTypes::Minimum => 70
];
$bed2->connection_type = RESTful::class;
$bed2->url = 'http://localIP/bed2';
$bed2->credentials = [];
$bed2->value = 54.7;

$hallway = new \stdClass();
$hallway->id = 10;
$hallway->name = 'Hallway';
$hallway->type = SmokeDetector::class;
$hallway->thresholds = [
    ThresholdTypes::Minimum => 70
];
$hallway->connection_type = RESTful::class;
$hallway->url = 'http://localIP/hallway';
$hallway->credentials = [];
$hallway->value = 75.4;

$wineroom = new \stdClass();
$wineroom->id = 11;
$wineroom->name = 'Wine Room';
$wineroom->type = Temperature::class;
$wineroom->thresholds = [
    ThresholdTypes::Maximum => 73,
    ThresholdTypes::Minimum => 50
];
$wineroom->connection_type = RESTful::class;
$wineroom->url = 'http://localIP/wineroom';
$wineroom->credentials = [];
$wineroom->value = 68.5;

return [
    $kitchen,
    $basement,
    $front,
    $back,
    $basement_glass,
    $main,
    $upstairs,
    $bed1,
    $bed2,
    $hallway,
    $wineroom
];