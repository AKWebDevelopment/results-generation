<?php

class AbstractServiceTest extends \PHPUnit\Framework\TestCase
{
    protected $sensors, $stub;

    public function setUp()
    {
        $this->sensors = require __DIR__.'/../sensor_seed.php';
        $this->stub = $this->getMockForAbstractClass(\App\Services\AbstractService::class);
    }

    /** @test */
    public function it_should_run_the_current_method_on_the_abstract_class()
    {
        // Should make API call to get the current value
        $this->stub->expects($this->once())
            ->method('get_current')
            ->willReturn(45);

        $result = $this->stub->current($this->sensors[0]);

        // Result should be a translated sensor.
        $this->assertInstanceOf($this->sensors[0]->type, $result);
    }

    /** @test */
    public function it_should_transform_the_sensor_input()
    {
        $result = $this->stub->transform($this->sensors[3], 777);

        $this->assertInstanceOf($this->sensors[3]->type, $result);
    }

    /** @test */
    public function it_should_iterate_get_current_for_synchronous()
    {
        $this->stub->expects($this->exactly(count($this->sensors)))
            ->method('get_current')
            ->willReturn(rand(25, 90));

        $this->stub->currentInArray($this->sensors);
    }
}