<?php

use App\Controllers\DashboardController;

function view($template, $arguments)
{
    return ['template' => $template, 'arguments' => $arguments];
}

class DashboardControllerTest extends \PHPUnit\Framework\TestCase
{
    protected $sensors, $controller;

    public function setUp()
    {
        require_once __DIR__.'/../../src/helpers.php';
        $this->sensors = require __DIR__.'/../sensor_seed.php';
        $this->controller = new DashboardController();
    }

    /** @test */
    public function it_should_show_the_main_dashboard()
    {
        $res = $this->controller->index();

        $this->assertEquals($res['template'], 'dashboard.tpl');
        $this->assertArrayHasKey('sensors', $res['arguments']);
        $this->assertEquals(count($this->sensors), count($res['arguments']['sensors']));
    }

    /** @test */
    public function it_should_show_a_single_item()
    {
        $item = $this->sensors[array_rand($this->sensors)];
        $res = $this->controller->show($item->id);

        $this->assertEquals($res['template'], 'form.tpl');
        $this->assertInstanceOf(\App\Sensors\SensorInterface::class, $res['arguments']['sensor']);
        $this->assertEquals($res['arguments']['sensor']->id, $item->id);
    }

    /** @test */
    public function it_should_allow_posting_arbitrary_values()
    {
        $res = $this->controller->show(6, 55);

        $this->assertEquals($res['template'], 'form.tpl');
        $this->assertInstanceOf(\App\Sensors\SensorInterface::class, $res['arguments']['sensor']);
        $this->assertEquals(6, $res['arguments']['sensor']->id);
    }
}