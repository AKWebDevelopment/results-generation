<?php

class DoorTest extends \PHPUnit\Framework\TestCase
{
    public $sensor, $stub;

    public function setUp()
    {
        $this->sensor = new stdClass();
        $this->sensor->id = 42;
        $this->sensor->thresholds = [
            \App\Enums\ThresholdTypes::Boolean => false
        ];
        $this->stub = new \App\Sensors\Door($this->sensor);
    }

    /** @test */
    public function it_should_property_tell_if_acceptable()
    {
        $door = new \App\Sensors\Door($this->sensor, true);
        $this->assertFalse($door->acceptable());

        $door->setValue(false);
        $this->assertTrue($door->acceptable());
    }

    /** @test */
    public function it_should_properly_format_the_value()
    {
        $door = new \App\Sensors\Door($this->sensor, false);
        $this->assertEquals('Closed', $door->formattedValue());

        $door->setValue(true);
        $this->assertEquals('Open', $door->formattedValue());
    }

    /** @test */
    public function it_should_have_a_type_name()
    {
        $this->assertEquals('Door', $this->stub->typeString());
    }
}