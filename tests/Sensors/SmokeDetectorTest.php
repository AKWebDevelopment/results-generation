<?php

class SmokeDetectorTest extends \PHPUnit\Framework\TestCase {
    public $sensor;

    public function setUp()
    {
        $this->sensor = new stdClass();
        $this->sensor->id = 42;
        $this->sensor->thresholds = [
            \App\Enums\ThresholdTypes::Minimum => 70
        ];
    }

    /** @test */
    public function it_should_property_tell_if_acceptable()
    {
        $smoke = new \App\Sensors\SmokeDetector($this->sensor, 55.1);
        $this->assertFalse($smoke->acceptable());

        $smoke->setValue(80.6);
        $this->assertTrue($smoke->acceptable());
    }

    /** @test */
    public function it_should_properly_format_the_value()
    {
        $smoke = new \App\Sensors\SmokeDetector($this->sensor, 75);
        $this->assertEquals('75.0% Visibility', $smoke->formattedValue());
    }

    public function it_should_say_smoke_as_type()
    {
        $smoke = new \App\Sensors\SmokeDetector($this->sensor);
        $this->assertEquals('Smoke', $smoke->type());
    }
}