<?php

class TemperatureTest extends \PHPUnit\Framework\TestCase {
    public $sensor;

    public function setUp()
    {
        $this->sensor = new stdClass();
        $this->sensor->id = 42;
        $this->sensor->thresholds = [
            \App\Enums\ThresholdTypes::Minimum => 45,
            \App\Enums\ThresholdTypes::Maximum => 73
        ];
    }

    /** @test */
    public function it_should_property_tell_if_acceptable()
    {
        // Too High
        $temp = new \App\Sensors\Temperature($this->sensor, 75);
        $this->assertFalse($temp->acceptable());

        // Too Low
        $temp->setValue(44);
        $this->assertFalse($temp->acceptable());

        // Just Right
        $temp->setValue(56);
        $this->assertTrue($temp->acceptable());
    }

    /** @test */
    public function it_should_properly_format_the_value()
    {
        $temp = new \App\Sensors\Temperature($this->sensor, 70);
        $this->assertEquals('70.0 Degrees', $temp->formattedValue());
    }

    public function it_should_say_proper_type()
    {
        // Fires
        unset($this->sensor->thresholds[\App\Enums\ThresholdTypes::Minimum]);
        $temp = new \App\Sensors\Temperature($this->sensor);
        $this->assertEquals('Fire', $temp->typeString());

        // Freeze
        $this->sensor->thresholds = [\App\Enums\ThresholdTypes::Minimum => 45];
        $temp = new \App\Sensors\Temperature($this->sensor);
        $this->assertEquals('Freeze', $temp->typeString());

        // Fire/Freeze
        $this->sensor->thresholds[\App\Enums\ThresholdTypes::Maximum] = 75;
        $temp = new \App\Sensors\Temperature($this->sensor);
        $this->assertEquals('Fire/Freeze', $temp->typeString());
    }
}