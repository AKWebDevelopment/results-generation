<?php

namespace Tests;

class TestInheritor extends \App\Sensors\AbstractSensor {
    public $Maximum, $Minimum, $Boolean, $value;

    public function acceptable(): bool
    {
        $this->checkValue();
        return true;
    }

    public function formattedValue(): string
    {
        $this->checkValue();
        return 'hi';
    }

}

class AbstractSensorTest extends \PHPUnit\Framework\TestCase
{
    protected $sensors, $thresholds, $stub;

    public function setUp()
    {
        $this->sensors = require __DIR__.'/../sensor_seed.php';
        $this->stub = new TestInheritor($this->sensors[0]);
    }

    /** @test */
    public function it_sets_up_constructor_values()
    {
        // Thresholds
        foreach($this->sensors[0]->thresholds as $key => $value)
        {
            $attribute = \App\Enums\ThresholdTypes::keyByValue($key);
            $this->assertEquals($value, $this->stub->$attribute);
        }

        // id
        $this->assertEquals($this->sensors[0]->id, $this->stub->id);
    }

    /** @test */
    public function it_should_accept_a_value_from_either_construct_or_explicit_set()
    {
        // By default, the $value attribute is protected, but we made public in the test here for ease.
        // Also by default it should be null.
        $this->assertNull($this->stub->value);

        $this->stub->setValue(42);
        $this->assertEquals(42, $this->stub->value);

        // Optional constructor setting
        $this->stub = new TestInheritor($this->sensors[0], 99);
        $this->assertEquals(99, $this->stub->value);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_should_throw_exception_if_no_value_is_set_before_acceptable()
    {
        $this->stub->acceptable();
    }

    /** @test */
    public function it_should_not_throw_an_exception_if_value_is_set_before_acceptable()
    {
        $this->stub->setValue(42);
        $this->assertTrue($this->stub->acceptable());
    }

    /** @test */
    public function it_should_by_default_return_the_class_name_for_type()
    {
        $this->assertEquals('TestInheritor', $this->stub->typeString());
    }
}