<?php

class GlassBreakTest extends \PHPUnit\Framework\TestCase {

    public $sensor;

    public function setUp()
    {
        $this->sensor = new stdClass();
        $this->sensor->id = 42;
        $this->sensor->thresholds = [
            \App\Enums\ThresholdTypes::Maximum => 300
        ];
    }

    /** @test */
    public function it_should_property_tell_if_acceptable()
    {
        $sensor = new \App\Sensors\GlassBreak($this->sensor, 700);
        $this->assertFalse($sensor->acceptable());

        $sensor->setValue(150);
        $this->assertTrue($sensor->acceptable());
    }

    /** @test */
    public function it_should_properly_format_the_value()
    {
        $sensor = new \App\Sensors\GlassBreak($this->sensor, 175.57);
        $this->assertEquals('175.570hz', $sensor->formattedValue());
    }

    /** @test */
    public function it_should_return_glass_break_as_the_type()
    {
        $sensor = new \App\Sensors\GlassBreak($this->sensor);
        $this->assertEquals('GlassBreak', $sensor->typeString());
    }
}