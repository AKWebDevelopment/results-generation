<?php

include __DIR__.'/../src/helpers.php';

class HelpersTest extends \PHPUnit\Framework\TestCase {

    /** @test */
    public function it_should_return_the_root_directory_path()
    {
        // Default
        $this->assertEquals(realpath(__DIR__.'/../'), root_path());

        // Append
        $this->assertEquals(realpath(__DIR__.'/../src/Enums/ThresholdTypes.php'), root_path('src/Enums/ThresholdTypes.php'));
    }

    /** @test */
    public function it_should_group_array_by_attributes()
    {
        $obj1 = new stdClass();
        $obj1->id = 1;
        $obj1->type = 'a';

        $obj2 = new stdClass();
        $obj2->id = 2;
        $obj2->type = 'b';

        $obj3 = new stdClass();
        $obj3->id = 3;
        $obj3->type = 'b';

        $obj4 = new stdClass();
        $obj4->id = 4;
        $obj4->type = 'a';

        $result = group_by([$obj1, $obj2, $obj3, $obj4], 'type');

        $this->assertArrayHasKey('a', $result);
        $this->assertArrayHasKey('b', $result);
        $this->assertContains($obj1, $result['a']);
        $this->assertContains($obj2, $result['b']);
        $this->assertNotContains($obj1, $result['b']);
        $this->assertNotContains($obj2, $result['a']);
    }
}