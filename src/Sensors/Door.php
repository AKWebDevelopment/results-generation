<?php

namespace App\Sensors;

class Door extends AbstractSensor implements SensorInterface {

    protected $Boolean;

    public function acceptable(): bool
    {
        $this->checkValue();
        return $this->value == $this->Boolean;
    }

    public function formattedValue(): string
    {
        return ($this->value)
            ? 'Open'
            : 'Closed';
    }

}