<?php

namespace App\Sensors;

class Temperature extends AbstractSensor implements SensorInterface {

    protected $Minimum, $Maximum;

    public function acceptable(): bool
    {
        $this->checkValue();
        if(!empty($this->Maximum) && $this->value > $this->Maximum)
        {
            return false;
        }

        if(!empty($this->Minimum) && $this->value < $this->Minimum)
        {
            return false;
        }

        return true;
    }

    public function formattedValue(): string
    {
        $this->checkValue();
        return number_format($this->value, 1).' Degrees';
    }

    public function typeString(): string
    {
        $return = '';
        if(!empty($this->Maximum))
        {
            $return = 'Fire';
        }

        if(!empty($this->Minimum))
        {
            if(strlen($return))
            {
                $return .= '/';
            }

            $return .= 'Freeze';
        }

        return $return;
    }

}