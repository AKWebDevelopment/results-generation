<?php

namespace App\Sensors;

class GlassBreak extends AbstractSensor implements SensorInterface {

    protected $Maximum;

    public function acceptable(): bool
    {
        $this->checkValue();
        return $this->value <= $this->Maximum;
    }

    public function formattedValue(): string
    {
        $this->checkValue();
        return number_format($this->value, 3).'hz';
    }

}