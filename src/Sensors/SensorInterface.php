<?php

namespace App\Sensors;

interface SensorInterface
{
    public function __construct($thresholds);
    public function setValue($value);
    public function acceptable(): bool;
    public function formattedValue(): string;
    public function typeString(): string;
}