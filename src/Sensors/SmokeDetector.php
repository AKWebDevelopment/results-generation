<?php

namespace App\Sensors;

class SmokeDetector extends AbstractSensor  implements SensorInterface {

    protected $Minimum;

    public function acceptable(): bool
    {
        $this->checkValue();
        return $this->value >= $this->Minimum;
    }

    public function formattedValue(): string
    {
        $this->checkValue();
        return number_format($this->value, 1).'% Visibility';
    }

    public function type(): string
    {
        return 'Smoke';
    }
}