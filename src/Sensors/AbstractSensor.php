<?php

namespace App\Sensors;

use App\Enums\ThresholdTypes;

abstract class AbstractSensor implements SensorInterface {

    protected $value;
    public $id, $sensor;

    public function __construct($sensor, $value = null)
    {
        $this->sensor = $sensor;

        foreach($sensor->thresholds as $key => $val)
        {
            $type = ThresholdTypes::keyByValue($key);
            if(property_exists($this, $type))
            {
                $this->$type = $val;
            }
        }

        $this->id = $sensor->id;
        $this->value = $value;
    }

    protected function checkValue()
    {
        if(is_null($this->value))
        {
            throw new \InvalidArgumentException('The value of a reading must be set before it can be determined if it is acceptable.');
        }
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function typeString(): string
    {
        $namespaces = explode("\\", get_class($this));
        return end($namespaces);
    }

    public function __get($name)
    {
        if(property_exists($this->sensor, $name))
        {
            return $this->sensor->$name;
        } else {
            throw new \Exception('The property '.$name.' does not exist on the '.__CLASS__.' class.');
        }
    }
}