<?php

use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::get('/', 'DashboardController@index');
SimpleRouter::get('/sensors/{id}', 'DashboardController@show');
SimpleRouter::post('/sensors/{id}', 'DashboardController@update');