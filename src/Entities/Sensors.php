<?php

namespace App\Entities;

class Sensors
{
    public $id, $name, $type, $thresholds, $connection_type, $url, $value, $credentials;

    /**
     * Return an array of all sensors.
     * Normally this would be a DB abstraction, but creating an actual
     * DB is beyond the scope of this particular test.
     *
     * @return array
     */
    public static function all()
    {
        return include root_path('/tests/sensor_seed.php');
    }

    /**
     * Return the sensor with the given id.
     *
     * @param $id
     * @return mixed
     */
    public static function find($id)
    {
        $sensors = include root_path('/tests/sensor_seed.php');

        foreach($sensors as $s)
        {
            if($s->id == $id)
            {
                return $s;
            }
        }
    }

}