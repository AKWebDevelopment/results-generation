<?php

namespace App\Services;

use App\Entities\Sensors;

abstract class AbstractService implements SensorServiceInterface {

    /**
     * Simply let this class know if we can run all at the same time
     * or if we should iterate.
     */
    protected $supports_asynchronous = false;

    public function currentInArray(array $sensors)
    {
        $return = [];
        foreach($sensors as $sensor)
        {
            $return[] = $this->current($sensor);
        }
        return $return;
    }

    public function current($sensor)
    {
        $result = $this->get_current($sensor);

        return $this->transform($sensor, $result);
    }

    abstract protected function get_current($sensor_or_sensors);

    public function transform($sensor, $result)
    {
        $type = new $sensor->type($sensor);
        $type->setValue($result);

        return $type;
    }
}