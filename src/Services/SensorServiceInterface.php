<?php

namespace App\Services;

use App\Entities\Sensors;

interface SensorServiceInterface {

    public function currentInArray(array $sensors);

    public function current($sensor);

    public function transform($sensor, $results);

}