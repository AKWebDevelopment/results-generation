<?php

namespace App\Services;

class Soap extends AbstractService implements SensorServiceInterface {

    protected function get_current($sensor)
    {
        /**
         * Normally this method would package up the request into the proper
         * XML format and open a tunnel to the sensor via a SoapClient, and
         * then make a function call for the current value, but that is beyond
         * the scope of this project, so we are just returning the transformed
         * entity object.
         */
        return $sensor->value;
    }

}