<?php

namespace App\Services;

class FTP extends AbstractService  implements SensorServiceInterface {

    protected function get_current($sensor)
    {
        /*
         * Normally this method would connect to a remote FTP point, traverse
         * directories, and find the proper file, but that's out of scope for
         * this test, so we're just returning the value already on the entity.
         */
        return $sensor->value;
    }

}