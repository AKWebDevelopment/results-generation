<?php

namespace App\Services;

class RESTful extends AbstractService implements SensorServiceInterface {

    // Asynchronous requests are pretty easy with Guzzle for HTTP requests.
    protected $supports_asynchronous = true;

    /**
     * Get current values for given sensor(s).
     * Normally this method would set up making the RESTful JSON requests
     * and fire them off asynchronously if needed, but that's beyond this
     * test's scope, so we're just transforming based on the values provided
     * on the sensor objects themselves.
     *
     * @param $sensor
     * @return mixed
     */
    protected function get_current($sensor)
    {
        return $sensor->value;
    }

}