{extends file="layout.tpl"}

{block name="heading"}Dashboard{/block}

{block name="content"}
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>State</th>
                    <th>Alarm</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            {foreach $sensors as $sensor}
                {strip}
                    <tr class="{if $sensor->acceptable()}acceptable{else}alarm{/if}">
                        <td>{$sensor->name}</td>
                        <td>{$sensor->typeString()}</td>
                        <td>{$sensor->formattedValue()}</td>
                        <td>{if !$sensor->acceptable()}ALARM{/if}</td>
                        <td><a href="/sensors/{$sensor->id}" class="btn btn-xs btn-default pull-right">Details</a></td>
                    </tr>
                {/strip}
            {/foreach}
            </tbody>
        </table>
    </div>
{/block}