{extends file="layout.tpl"}

{block name="heading"}<a href="/" class="btn btn-default btn-lg">< Back</a> Sensor {$sensor->name} <small class="bg-{if $sensor->acceptable()}success">Acceptable{else}danger">ALARM{/if}</small>{/block}

{block name="content"}
    <div class="table-responsive">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" disabled class="form-control" id="name" name="name" value="{$sensor->name}" />
        </div>
        <div class="form-group">
            <label for="type">Type</label>
            <input type="text" disabled class="form-control" id="type" name="type" value="{$sensor->typeString()}" />
        </div>
        <div class="form-group">
            <label for="url">URL</label>
            <input type="text" disabled class="form-control" id="url" name="url" value="{$sensor->url}" />
        </div>
        <div class="form-group">
            <label for="connection_type">Connection Type</label>
            <input type="text" disabled class="form-control" id="connection_type" name="connection_type" value="{$connection_type}" />
        </div>
        <div class="form-group">
            <label for="thresholds">Thresholds</label>
            <div class="row">
                {foreach $sensor->thresholds as $key => $value}
                    {strip}
                        <div class="col-xs-3">
                            <input type="text" disabled class="form-control" id="threshold_key[]" name="threshold_key[]" value="{\App\Enums\ThresholdTypes::keyByValue($key)}" />
                        </div>
                        <div class="col-xs-9">
                            <input type="text" disabled class="form-control" id="threshold_val[]" name="threshold_val[]" value="{(string) $value}" />
                        </div>
                    {/strip}
                {/foreach}
            </div>
        </div>
    </div>
    <hr />
    <h3>Test Values</h3>
    {if isset($test)}
        <div class="alert alert-{if $sensor->acceptable()}success{else}danger{/if}">
            The test value <span style="text-decoration: underline;">{$sensor->value}</span> <strong>{if $sensor->acceptable()}passed{else}failed{/if}</strong> the thresholds.
        </div>
    {/if}
    <div class="table-responsive">
        <form method="post">
            <div class="form-group">
                <label for="value">Value</label>
                <input type="text" class="form-control" id="value" name="value" value="{$sensor->value}" />
            </div>
            <button type="submit" class="btn btn-success pull-right">Submit Test Value</button>
         </form>
    </div>
{/block}