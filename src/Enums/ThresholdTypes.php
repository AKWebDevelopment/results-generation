<?php

namespace App\Enums;

use AdamKelso\DoubleA\Enumerable;

class ThresholdTypes
{
    use Enumerable;

    const Maximum = 1;
    const Minimum = 2;
    const Boolean = 3;
}