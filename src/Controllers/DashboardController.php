<?php

namespace App\Controllers;

use App\Entities\Sensors;
use Pecee\Http\Request;

class DashboardController {

    public function index()
    {
        $sensors = Sensors::all();
        $groups = group_by($sensors, 'connection_type');

        $results = [];
        foreach($groups as $protocol => $items)
        {
            $service = new $protocol();
            $results = array_merge($results, $service->currentInArray($items));
        }

        return view('dashboard.tpl', ['sensors' => $results]);
    }

    public function show($id)
    {
        $sensor = Sensors::find($id);
        $service = new $sensor->connection_type();

        return view('form.tpl', [
            'sensor' => $service->current($sensor),
            'connection_type' => $this->splitNamespace($sensor->connection_type)
        ]);
    }

    public function update($id)
    {
        $sensor = Sensors::find($id);
        $service = new $sensor->connection_type();

        $value = Request::getInstance()->getInput()->get('value', null);

        $transformed = (!is_null($value))
            ? $service->transform($sensor, (float) $value)
            : $service->current($sensor);

        $sensor->value = $value;

        return view('form.tpl', [
            'sensor' => $transformed, 'test' => true,
            'connection_type' => $this->splitNamespace($sensor->connection_type)
        ]);
    }

    private function splitNamespace($connection_type)
    {
        $namespaces = explode("\\", $connection_type);
        return end($namespaces);
    }
}