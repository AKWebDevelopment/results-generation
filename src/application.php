<?php

use Pecee\SimpleRouter\SimpleRouter;

require_once 'routes.php';
require_once 'helpers.php';

// Set up routing
SimpleRouter::start('App\Controllers');