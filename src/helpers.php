<?php

if(!function_exists('root_path'))
{
    function root_path($path = '')
    {
        return realpath(__DIR__.'/../'.$path);
    }
}

if(!function_exists('view'))
{
    function view($template, $args = [])
    {
        $smarty = new Smarty();
        $smarty->setCompileDir(root_path('/storage/views'));
        $smarty->setTemplateDir(root_path('/src/Views'));

        foreach($args as $key => $value)
        {
            $smarty->assign($key, $value);
        }

        $smarty->display(root_path('/src/Views/'.$template));
    }
}

if(!function_exists('group_by'))
{
    function group_by($array, $attribute)
    {
        $groups = [];

        foreach($array as $item)
        {
            if(!isset($groups[$item->$attribute]))
            {
                $groups[$item->$attribute] = [];
            }

            $groups[$item->$attribute][] = $item;
        }

        return $groups;
    }
}